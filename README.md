# renewables-forecasting

Machine learning-based renewable energy forecasting and optimisation.

by Nithiya Streethran (nmstreethran at gmail dot com)

**This project is a work-in-progress. Feedback and suggestions are always welcome. Please refer to the contributing guidelines if you would like to contribute.**

## About

Due to the global transition into a low-carbon energy system, there is an increase in electrification and decentralisation of the energy system with renewable energy resources. Intermittent renewable energy resources, namely solar and wind, are supplying more and more of the electricity demand in Europe, with Germany having the highest share. Advancement in technology means that larger amounts of high-resolution data describing the electricity system will be collected, which can be exploited to better manage the intermittent system in the short term. This project will investigate how machine learning algorithms and publicly available electricity system and meteorological data can be used by wind and solar energy generating companies in Germany for short-term forecasting of generation, load, and market prices, and subsequently management of their portfolio and bids in the electricity market. In contrast to the proprietary nature of existing tools used by energy companies, this project will propose a fully open-source solution.

**Focus:**

- Forecasting of electricity generation, load, and market prices using regression algorithms
- Data for the first half of 2018
  - German meteorological measurements and power plants
  - Generation, load, and market prices for the DE-AT-LU bidding zone
- Initial analysis with wind energy

## Installing dependencies

Running scripts and building the documentation locally require installation of [Python 3](https://www.python.org/).

Once Python is installed, clone this repository (including datasets):

- *Option 1* - via HTTPS:

  ```sh
  git clone git clone --recurse-submodules --remote-submodules https://gitlab.com/renewables-forecasting/renewables-forecasting.git
  ```

- *Option 2* - via SSH:

  ```sh
  git clone git clone --recurse-submodules --remote-submodules git@gitlab.com:renewables-forecasting/renewables-forecasting.git
  ```

Then, navigate to the directory of the cloned repository:

```sh
cd renewables-forecasting
```

Finally, create and activate a virtual environment, and install all dependencies:

- *Option 1* - using [`venv`](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment):

  on Linux:

  ```sh
  python3 -m venv env
  source env/bin/activate
  python -m pip install -r requirements.txt
  ```

  on Windows:

  ```powershell
  py -3 -m venv env
  .\env\Scripts\activate
  py -m pip install -r requirements.txt
  ```

- *Option 2* - using [Anaconda](https://www.anaconda.com/products/individual) (I recommend the lightweight [Miniconda](https://docs.conda.io/en/latest/miniconda.html)):

  ```sh
  conda env create --file environment.yml
  conda activate renewables-forecasting
  ```

Note that while the required packages are installed in an Anaconda virtual environment using [conda-forge](https://conda-forge.org/), some packages may be outdated compared to the pip versions.

To view the full list of dependencies, see `requirements.txt`.

<!-- ## Documentation

Documentation files can be found in the `docs` folder. It will be hosted using GitLab Pages or Wiki in the future.

A Jupyter Notebook of visualisations can be viewed on [nbviewer](https://nbviewer.jupyter.org/urls/gitlab.com/renewables-forecasting/renewables-forecasting/-/raw/main/docs/notebook.ipynb). -->

## Data

Datasets used and their descriptions are available at <https://gitlab.com/renewables-forecasting/renewables-forecasting-data>.

Raw data can be accessed using the [GitLab API](https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository):

```text
GET /projects/:id/repository/files/:file_path/raw
```

The required parameters are `file_path`, which is [URL (percent) encoded](https://en.wikipedia.org/wiki/Percent-encoding) (e.g., `/` to `%2F`), and `ref`, which is the name of the branch, tag, or commit.

For example, the following URL points to `data/meteorology/stations.csv`:

```text
https://gitlab.com/api/v4/projects/19753809/repository/files/meteorology%2Fstations%2Ecsv/raw?ref=main
```

## License

Unless otherwise stated:

- Python scripts, Jupyter notebooks, and any other form of code or snippets in this repository are licensed under the [MIT License](https://opensource.org/licenses/MIT).
- content, images, and documentation are licensed under a [Creative Commons Attribution 4.0 International (CC-BY-4.0) License](https://creativecommons.org/licenses/by/4.0/).

## Credits

This repository is a continuation and improvement of the work done by Nithiya Streethran in [ENSYSTRA](https://ensystra.eu/). ENSYSTRA is funded by the European Union's Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No: 765515.

Project badges are generated using [Shields.io](https://shields.io/) and [Simple Icons](https://simpleicons.org/).
