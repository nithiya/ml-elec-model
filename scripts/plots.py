"""Plots

Data sources:

- electricityMap (https://github.com/tmrowco/electricitymap-contrib)
- nomenclature of territorial units for statistics (NUTS); Eurostat
  (https://ec.europa.eu/eurostat/web/nuts/background)
- Deutscher Wetterdienst (DWD)
  (https://www.dwd.de/EN/climate_environment/cdc/cdc_node.html)
- Natomraden.se (https://www.natomraden.se/)
- Erneuerbare-Energien-Gesetz (EEG); Netztransparenz.de
  (https://www.netztransparenz.de/EEG/Anlagenstammdaten)
- GeoNames (https://download.geonames.org/export/zip/)
"""

# %%
# import libraries
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
from bokeh.io import output_notebook
from bokeh.models import CategoricalColorMapper, GeoJSONDataSource
from bokeh.palettes import viridis
from bokeh.plotting import figure, show
from bokeh.tile_providers import CARTODBPOSITRON_RETINA, get_provider

# %%
# configure matplotlib plot styles
plt.style.use("Solarize_Light2")
plt.rcParams["font.family"] = "Source Sans Pro"
plt.rcParams["figure.dpi"] = 96
plt.rcParams["axes.grid"] = False
plt.rcParams["text.color"] = "darkslategrey"
plt.rcParams["axes.titlesize"] = "14"
plt.rcParams["axes.labelsize"] = "10"
plt.rcParams["axes.titleweight"] = "700"

# %%
# bokeh plot configurations
output_notebook()
tile_provider = get_provider(CARTODBPOSITRON_RETINA)

# %% [markdown]
# ## Countries

# %%
data = gpd.read_file("data/geography/polygons/countries.geojson")

# %%
base = data.plot(
    cmap="viridis",
    figsize=(7, 15),
    legend=True,
    column="CNTR_CODE",
    legend_kwds={"loc": "upper left"}
)
plt.title("Countries covered by the study area")
plt.text(20, 40.1, "Data: Eurostat")
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.show()

# %% [markdown]
# ## Bidding zones

# %%
data = gpd.read_file("data/geography/polygons/bidding_zones.geojson")

# %%
base = data.plot(
    cmap="viridis",
    figsize=(7, 15),
    legend=True,
    column="zone",
    legend_kwds={"loc": "upper left"}
)
plt.title("Bidding zones in the study area")
plt.text(9, 40.1, "Data: Eurostat; ElectricityMap; Natomraden.se")
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.show()

# %% [markdown]
# ## Meteorological stations

# %%
# import data
usecols = [
    "station_id", "station_height", "latitude", "longitude",
    "station_name", "state", "type"
]
data = pd.read_csv(
    "data/meteorology/stations.csv", encoding="utf-8", usecols=usecols
)

# convert coordinates to well known text
data["wkt"] = (
    "POINT (" + data["longitude"].astype(str) + " " +
    data["latitude"].astype(str) + ")"
)

# convert to geodataframe
data = gpd.GeoDataFrame(
    data, geometry=gpd.GeoSeries.from_wkt(data["wkt"]), crs="EPSG:4326"
)
data = data.drop(columns=["wkt"])

# reproject to web mercator
data = data.to_crs(3857)

# convert data source to GeoJSON
geo_source = GeoJSONDataSource(geojson=data.to_json())

# %%
# generate unique colours for each state
const = list(set(data["state"]))
palette = viridis(len(const))
color_map = CategoricalColorMapper(factors=const, palette=palette)

# define title and tooltips
TITLE = (
    "Meteorological stations in Germany. © DWD.de."
)
TOOLTIPS = [
    ("NAME", "@station_name"),
    ("HEIGHT", "@station_height"),
    ("COORDINATES", "(@longitude, @latitude)"),
    ("ID", "@station_id"),
    ("STATE", "@state"),
    ("TYPE", "@type")
]

# configure plot
p = figure(
    title=TITLE,
    tools="wheel_zoom, pan, reset, hover, save",
    x_axis_location=None,
    y_axis_location=None,
    tooltips=TOOLTIPS,
    x_axis_type="mercator",
    y_axis_type="mercator"
)
p.grid.grid_line_color = None
p.hover.point_policy = "follow_mouse"
p.circle(
    "x",
    "y",
    source=geo_source,
    size=5,
    line_width=0,
    fill_color={"field": "state", "transform": color_map}
)
p.add_tile(tile_provider)
show(p)

# %% [markdown]
# ## Wind generators

# %%
# import data
data = pd.read_csv("data/power/installed/wind_agg.csv", encoding="utf-8")

# convert coordinates to well known text
data["wkt"] = (
    "POINT (" + data["longitude"].astype(str) + " " +
    data["latitude"].astype(str) + ")"
)

# convert to geodataframe
data = gpd.GeoDataFrame(
    data, geometry=gpd.GeoSeries.from_wkt(data["wkt"]), crs="EPSG:4326"
)
data = data.drop(columns=["wkt"])

# reproject to web mercator
data = data.to_crs(3857)

# convert data source to GeoJSON
geo_source = GeoJSONDataSource(geojson=data.to_json())

# %%
# generate unique colours for each TSO
const = list(set(data["TSO"]))
palette = viridis(len(const))
color_map = CategoricalColorMapper(factors=const, palette=palette)

# define title and tooltips
TITLE = (
    "Wind power plants in Germany. © Netztransparenz.de."
)
TOOLTIPS = [
    ("POWER (kW)", "@installed_capacity"),
    ("PLACE", "@city_district"),
    ("STATE", "@state"),
    ("TSO", "@TSO"),
    ("COORDINATES", "(@longitude, @latitude)"),
    ("NEAREST MET STATION", "@station_id")
]

# configure plot
p = figure(
    title=TITLE,
    tools="wheel_zoom, pan, reset, hover, save",
    x_axis_location=None,
    y_axis_location=None,
    tooltips=TOOLTIPS,
    x_axis_type="mercator",
    y_axis_type="mercator"
)
p.grid.grid_line_color = None
p.hover.point_policy = "follow_mouse"
p.circle(
    "x",
    "y",
    source=geo_source,
    size=5,
    line_width=0,
    fill_color={"field": "TSO", "transform": color_map}
)
p.add_tile(tile_provider)
show(p)
